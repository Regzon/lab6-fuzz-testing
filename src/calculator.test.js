import count from "./calculator.js";
import * as assert from "assert";


describe('Calculator tests:', () => {
  test('Valid sum test', (done) => {
    let a = Math.random() * 100000;
    let b = Math.random() * 100000;
    expect(count(a, b, '+')).toEqual(a + b);
    done();
  });

  test('Valid sum float test', (done) => {
    let a = 2.5;
    let b = 11.32;
    assert.equal(count(a, b, '+'), 2.5 + 11.32);
    done();
  });

  test('Valid sum negative test', (done) => {
    let a = 2.5;
    let b = -11.32;
    assert.equal(count(a, b, '+'), 2.5 + (-11.32));
    done();
  });

  test('Valid sub test', (done) => {
    let a = Math.random() * 100000;
    let b = Math.random() * 100000;
    assert.equal(count(a, b, '-'), a - b);
    done();
  });

  test('Valid sub float test', (done) => {
    let a = 7.3;
    let b = 2.1;
    assert.equal(count(a, b, '-'), 7.3 - 2.1);
    done();
  });

  test('Valid negative float test', (done) => {
    let a = 7.3;
    let b = 11.1;
    assert.equal(count(a, b, '-'), 7.3 - 11.1);
    done();
  });


  test('Valid div test', (done) => {
    let a = Math.random() * 100000;
    let b = Math.random() * 100000;
    assert.equal(count(a, b, '/'), a / b);
    done();
  });

  test('Valid div float test', (done) => {
    let a = 22;
    let b = 33;
    assert.equal(count(a, b, '/'), 22 / 33);
    done();
  });

  test('Valid div float negative test', (done) => {
    let a = -22;
    let b = 33;
    assert.equal(count(a, b, '/'), -22 / 33);
    done();
  });

  test('Valid mult test', (done) => {
    let a = Math.random() * 100000;
    let b = Math.random() * 100000;
    assert.equal(count(a, b, '*'), a * b)
    done();
  });

  test('Valid mult float test', (done) => {
    let a = 0.5;
    let b = 0.7;
    assert.equal(count(a, b, '*'), 0.5 * 0.7)
    done();
  });

  test('Valid mult float double negative test', (done) => {
    let a = -0.5;
    let b = -0.7;
    assert.equal(count(a, b, '*'), 0.5 * 0.7)
    done();
  });

  test('Valid mult float negative test', (done) => {
    let a = -0.5;
    let b = 0.7;
    assert.equal(count(a, b, '*'), -0.5 * 0.7)
    done();
  });

  test('Not valid div test', (done) => {
    let a = Math.random() * 100000;
    let b = 0;
    assert.equal(count(a, b, '/'), "Division by zero!");
    done();
  });
});
