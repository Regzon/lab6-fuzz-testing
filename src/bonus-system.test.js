import { calculateBonuses } from "./bonus-system.js"

describe('Bonus system tests:', () => {
  test('Test non-existing program', (done) => {
    const bonus = calculateBonuses("Invalid", 100);
    expect(bonus).toBe(0);
    done();
  });

  test('Test standard program', (done) => {
    const bonus = calculateBonuses("Standard", 100);
    expect(bonus).toBe(0.05);
    done();
  });

  test('Test premium program', (done) => {
    const bonus = calculateBonuses("Premium", 100);
    expect(bonus).toBe(0.1);
    done();
  });
  
  test('Test diamond program', (done) => {
    const bonus = calculateBonuses("Diamond", 100);
    expect(bonus).toBe(0.2);
    done();
  });

  test('Test <10000 bonus boundaries', (done) => {
    const bonusJustBefore = calculateBonuses("Premium", 9999);
    expect(bonusJustBefore).toBe(1 * 0.1);

    const bonusJustAfter = calculateBonuses("Premium", 10000);
    expect(bonusJustAfter).not.toBe(1 * 0.1);

    const bonusInMiddle = calculateBonuses("Premium", 5000);
    expect(bonusInMiddle).toBe(1 * 0.1);

    done();
  });

  test('Test <50000 bonus boundaries', (done) => {
    const bonusJustBefore = calculateBonuses("Premium", 49999);
    expect(bonusJustBefore).toBe(1.5 * 0.1);

    const bonusJustAfter = calculateBonuses("Premium", 50000);
    expect(bonusJustAfter).not.toBe(1.5 * 0.1);

    const bonusInMiddle = calculateBonuses("Premium", 30000);
    expect(bonusInMiddle).toBe(1.5 * 0.1);

    done();
  });

  test('Test <100000 bonus boundaries', (done) => {
    const bonusJustBefore = calculateBonuses("Premium", 99999);
    expect(bonusJustBefore).toBe(2 * 0.1);

    const bonusJustAfter = calculateBonuses("Premium", 100000);
    expect(bonusJustAfter).not.toBe(2 * 0.1);

    const bonusInMiddle = calculateBonuses("Premium", 70000);
    expect(bonusInMiddle).toBe(2 * 0.1);

    done();
  });

  test('Test >=100000 bonus boundaries', (done) => {
    const bonusJustBefore = calculateBonuses("Premium", 99999);
    expect(bonusJustBefore).not.toBe(2.5 * 0.1);

    const bonusJustAfter = calculateBonuses("Premium", 100000);
    expect(bonusJustAfter).toBe(2.5 * 0.1);

    const bonusInMiddle = calculateBonuses("Premium", 150000);
    expect(bonusInMiddle).toBe(2.5 * 0.1);

    done();
  });

});
